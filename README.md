# phishing-templates

Various phishing templates, both email templates and phishing site templates

## Email Templates

#### Office365 Protected Message
Legitimate Office365 "protected message" template ripped straight from Microsoft themselves. See https://twitter.com/MegabitMeghan/status/1415719844511633417

![image](/uploads/39826fe70be3f4e9f4238a2febd3c67f/image.png)

## Website Templates

#### Office365 Login Page
A two stage phishing site designed to first capture credentials then a page to serve a payload download. Credential capture is designed to mimic Office365 and the payload download page is generic and supposed to be modified for each client.

![image](/uploads/ee9a65926f080996fec46fac837435c3/image.png)

![image](/uploads/441c8541596c7ced542d277be55350d5/image.png)




